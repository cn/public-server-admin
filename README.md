This is a little project we did on automating and runnning Kubernetes with Rancher
and Ansible.

Read more on [our blog article](https://blog.neze.fr/projects/ansible-rancher-kubernetes/)!

# Inventory setup

You need access to your servers, and ansible needs ssh keys.

In the `hosts` file, the server called `backup` should be directly available
thanks to your ssh config. For the main server though, you generally provide
a public key when you install the OS. Ansible needs access to the ssh private
key.

What we did is
* Generate a private key
* Generate a small secret to encrypt sensitive information
* Encrypt the private key
* Put the encrypted private key only in the git repository

```sh
λ ssh-keygen -t rsa -b 2048 -C "root@example.com" -N "" -f id_rsa
λ openssl rand -hex 128 > secret.key
λ cat << EOF >> .gitignore
id_rsa
secret.key
EOF
λ source env-setup
λ ansible-vault --vault-id=secret.key encrypt --output=id_rsa.enc id_rsa
λ git add .gitignore id_rsa.enc id_rsa.pub
```

# Running ansible and Tasks setup

The whole playbook can be run with the `install` script.

```sh
λ ./install
```

However it contains several tasks and it is advised to run them separately.

## Basic configuration of the server

This can be run by specifying tags to the `install` script.

```sh
λ ./install --tags basic
```

This step contains the following tasks.

### Initialization of the server

The **init** task in `roles/init` basically ensures that the correct python
version is available in the server for ansible to work.

### Packages management

The **packages** task in `roles/packages` installs common packages with the
`aptitude` package manager and python libraries with `pip`.

To install docker it is necessary to first add the repository and the docker
code signing key as explained in their [install tutorial](https://docs.docker.com/install/linux/docker-ce/ubuntu/).

### Users management

The **users** task in `roles/users` manages users, their passwords and their
authorized SSH public key.

In the configuration file `roles/users/vars/main.yml` there should be at least
the root user and the public key that you generated in `id_rsa.pub`.

Additional users should be created to avoid SSH root login.

The user passwords (if any) are given in hashed format to be safe. This can
be done using many methods found by looking up how to "generate sha-512 hashed
password for linux". Among these methods are `mkpasswd`.

```sh
λ mkpasswd -m sha-512
```

### SSH Server configuration

The **sshd** task in `roles/sshd` ensures that the SSH server is up to date and
well configured.

The configuration file is a template in `roles/sshd/templates` and is configured
with variables, defaults in `roles/sshd/defaults/main.yml` and overrides in
`roles/sshd/vars/main.yml`.

Note that a variable was overriden to forbid password SSH access. We could also
forbid root access but ansible accesses the server with the root account and we
would need a more complicated setup to have it change the account it's using
after setting up the SSH server.

### Web server proxy

We want the user interface of the Rancher service (to come) to be available.
For this we have to setup a web server (nginx) on the machine and have it
proxy from a public address to the local rancher service.

The **proxy** task sets up the nginx proxy and provides SSL certificates thanks
to [LetsEncrypt](https://letsencrypt.org) in order to make the future interface
available in https.

The config file shows three variables.

* The local endpoint where we will have Rancher running. This will have to be
  corresponding to the Rancher setup.
* The public address of the website we want. This will need to be associated
  to a correct DNS record.
* The e-mail of the administrator to register to LetsEncrypt.

### Certificates backup

Doing many reinstallations is bad with LetsEncrypt, and they limit the number
of certificates they issue per week and per registrar.

A simple way of avoiding problems is by backing up the LetsEncrypt certificates.
The **backup** task basically backs up the **remote** letsencrypt folder by
saving it to a **local** archive and uploading this archive to the **destination**
backup server.

In our case we chose a server (configured in `hosts`) we all had SSH access to
and a destination folder (`/home/shared` configured in `roles/backup/vars/main.yml`)
we all had read and write access to.

## Running Rancher

The **rancher** task is quite complex. Its first part is about running rancher.

It can be run with the `install` script.

```sh
λ ./install --tags rancher --skip-tags in_kubernetes
```

The configuration of the `agent` and `server` sections in `roles/rancher/vars/main.yml`
specifies dockers parameters. Especially, the port and hostname should be
corresponding to the ones you configured in the proxy.

The ansible script sets up authentication. For this it needs access to the clear
text password of the Rancher admin user. Set up these parameters in the `admin`
section. The encrypted password can be obtained with ansible-vault.

```sh
λ source env-setup
λ ansible-vault --vault-id secret.key encrypt_string
**** type your password then [enter] then [ctrl+d] ****
```

Then, a Kubernetes environment is created on Rancher. Its name is configured
in the `kubernetes` section of the variables.

The Ansible script then register a host to the Kubernetes instance. The proper
registration is made by running a docker container. This is done with the ansible
`shell` module but could be done with the `docker` module.

Next step is basically about installing KubeControl and is setup in the same
task.

```sh
λ ./install --tags kubernetes
```

The automation is not properly setup here (needs some improvement), which is why
it is advised to run these steps separately and wait between them for the Kubernetes
instance to be properly up.

The last part of the **rancher** task installs kubectl, gets a token from Rancher
and transforms it into the corresponding token for the Kubernetes api.

## Running the sample service in Kubernetes

The **helloacn** task does that and can be run in the install script as well.

```sh
λ ./install --tags helloacn
```

In the variables file `roles/helloacn/vars/main.yml` you can set up the number
of replicas or the external port you want your service to be available on.

Basically there is no kubectl built-in role in ansible so the first task is
to figure out what the status of our service is (deployed or not, exposed,
correctly scaled) and to do only the necessary tasks among the three next tasks
(deploy, expose, scale).
